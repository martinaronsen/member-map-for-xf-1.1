memberMap = 
{
	map: null,
	zoomLevel: null,
	markerClusterer: null,
	markerClustererExist: null,
	
	markers: null,
	
	icons: [],
	infoWindow: null,
	info: null,
	currentPlace: null,
	
	bounds: null,

	init: function( markers )
	{
		var options = {
			'zoom': ( memberMap.zoomLevel || 2 ),
			'mapTypeId': google.maps.MapTypeId.ROADMAP
		};
		
		
		memberMap.map = new google.maps.Map( $( '#mapCanvas' )[0], options );
		memberMap.info = markers;
		
		/* Set a height of the map that fits our browser height */
		memberMap.setMapHeight();
		
		/* And adjust it if we resize our browser */
		$( window ).resize( memberMap.setMapHeight );
		
		memberMap.icons['unselected'] 	= new google.maps.MarkerImage( 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&chco=FFFFFF,008CFF,000000&ext=.png', new google.maps.Size(24, 32) );
		memberMap.icons['selected'] 	= new google.maps.MarkerImage( 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&chco=FFFFFF,42aaff,000000&ext=.png', new google.maps.Size(24, 32) );
		
		memberMap.markerClustererExist = ( $.isFunction( window.MarkerClusterer ) ) ? true : false;
		
		if ( memberMap.markerClustererExist && $( '#useMC' ) )
		{
			google.maps.event.addDomListener( $( '#useMC' )[0], 'click', memberMap.toggleMC );
		}
		
		memberMap.showMarkers();
	},

	setZoomLevel: function( zoomLevel )
	{
		memberMap.zoomLevel = parseInt( zoomLevel );
	},
	
	setMapHeight: function()
	{
		browserHeight = $( window ).height();
		moderatorBar  = ( $( '#moderatorBar' ).outerHeight( true ) || 0 );
		headerHeight  = $( '#headerProxy' ).outerHeight( true );
		footerHeight  = $( '.footer' ).outerHeight( true ) + $( '.footerLegal' ).outerHeight( true );
		contentHeight = $( '.titleBar' ).outerHeight( true ) + $( '.breadBoxTop' ).outerHeight( true ) + $( '.breadBoxBottom' ).outerHeight( true );
		
		totalPageHeight = moderatorBar + headerHeight + footerHeight + contentHeight;
		leftForMe 		= browserHeight - totalPageHeight + 20;
		
		$( '#mainWrapper' ).css( { height: leftForMe + 'px' } );
		$( '#mapWrapper' ).css( { height: ( leftForMe - 15 ) + 'px' } );
		$( '#markerlist' ).css( { 'max-height': leftForMe + 'px' } );
	},

	showMarkers: function()
	{
		memberMap.markers = [];
		
		if ( memberMap.markerClustererExist && $( '#useMC' ).is( ':checked' ) == false && memberMap.markerClusterer )
		{
			memberMap.markerClusterer.clearMarkers();
		}


		var showFollowedUsersBlock 	= false;
		var showStaffMembersBlock	= false;
		var showOtherUsersBlock		= false;
		
		$.each( memberMap.info, function() 
		{
			var titleText = this.username;
			var title = $( '<li>' ).attr( { className: 'title' } ).append( titleText );
			
			if ( this.staffMember == true )
			{
				$( '#staffMembers ul' ).append( title );
				showStaffMembersBlock = true;
			}
			else if ( this.followedUser == true )
			{
				$( '#followedUsers ul' ).append( title );
				showFollowedUsersBlock = true;
			}
			else
			{
				$( '#unfollowedUsers ul' ).append( title );
				showOtherUsersBlock = true;
			}

			var latLng = new google.maps.LatLng( this.latitude, this.longitude );

			memberMap.bounds.extend( latLng );

			var mapMarker = new google.maps.Marker({
				'position': latLng,
				'icon': memberMap.icons['unselected'],
				'title': titleText
			});
			
			var fn = memberMap.markerClickFunction( this, mapMarker, latLng );
			google.maps.event.addListener( mapMarker, 'click', fn );

			$( title ).click( fn );
			
			memberMap.markers.push( mapMarker );
			mapMarker.setMap( memberMap.map );
			/*if ( ! memberMap.markerClustererExist || $( '#useMC' ).is( ':checked' ) == false )
			{
				mapMarker.setMap( memberMap.map );
			}*/
		});
		
		if ( memberMap.markerClustererExist && $( '#useMC' ).is( ':checked' ) == true )
		{
			memberMap.markerClusterer = new MarkerClusterer( memberMap.map, memberMap.markers );
		}
		
		/* Show the sidebar blocks, if we have content in them */
		if ( showStaffMembersBlock )
		{
			$( '#staffMembers' ).slideDown( XenForo.speed.fast );
		}
		
		if ( showFollowedUsersBlock )
		{
			$( '#followedUsers' ).slideDown( XenForo.speed.fast );
		}
		
		if ( showOtherUsersBlock )
		{
			$( '#unfollowedUsers' ).slideDown( XenForo.speed.fast );
			
			/* Don't show the title unless there are staff members or users you follow in any other blocks */
			if ( showStaffMembersBlock || showFollowedUsersBlock )
			{
				$( '#unfollowedUsers h3' ).show();
			}
		}
		
		if ( memberMap.bounds != null )
		{
			memberMap.map.setCenter( memberMap.bounds.getCenter() );
			//memberMap.map.fitBounds( memberMap.bounds );
			//memberMap.map.setZoom( 2 );
		}
	},
	
	toggleMC: function()
	{
		memberMap.clear();
		
		memberMap.showMarkers();
	},
	
	clear: function()
	{
		$.each( memberMap.markers, function() { this.setMap(null); } );
		
		if ( memberMap.markerClustererExist && $( '#useMC' ).is( ':checked' ) && memberMap.markerClusterer != null )
		{
			memberMap.markerClusterer.clearMarkers();
		}
		
		/* Clear the content of the sidebar. This will be rebuilt in memberMap.showMarkers() */
		$( '#staffMembers' ).hide();
		$( '#staffMembers ul' ).empty();
		$( '#followedUsers' ).hide();
		$( '#followedUsers ul' ).empty();
		$( '#unfollowedUsers' ).hide().next( 'h3').hide();
		$( '#unfollowedUsers ul' ).empty();
		
	},
	
	markerClickFunction: function( info, mapMarker, latLng )
	{
		return function(e)
		{
			var hidingMarker = memberMap.currentPlace;

			var slideIn = function( info ) 
			{
				$( 'h1 a', $( '#mapDetails' ) ).text( info.username );
				$( 'h1 a', $( '#mapDetails') ).attr( 'href', info.profileLink );
				$( 'p',  $( '#mapDetails' ) ).text( info.location );
				$( 'img', $( '#mapDetails') ).attr( 'src', info.profilePic );
				$( '#mapDetails' ).animate({ right: '10px' } );
				
				if ( memberMap.map.getZoom() <= 6 )
				{
					memberMap.map.setZoom( 6 );
				}
				
				memberMap.map.panTo( latLng );
				//memberMap.map.setCenter( latLng );
			}
			
			mapMarker.setIcon( memberMap.icons['selected'] );
			
			if ( memberMap.currentPlace ) 
			{
				memberMap.currentPlace.setIcon( memberMap.icons['unselected'] );
			
				$( '#mapDetails' ).animate(
						{ 
							right: '-320px' 
						},
						{ 
							complete: function() 
							{
								if ( hidingMarker != mapMarker ) {
									slideIn( info );
								}
								else
								{
									memberMap.currentPlace = null;
								}
							}
						}
				);
			} 
			else 
			{
			  slideIn( info );
			}
			
			memberMap.currentPlace = mapMarker;
		}
	}
};
/** @param {jQuery} $ jQuery Object */
!function($, window, document, _undefined)
{
	// new namespace for this add-on: XenForo.memberMap
	XenForo.memberMapAjax =
	{
		deleteLocation: function( button )
		{
			button.click( $( '#MarkerForm' ).submit() );
		},
		
		locationSearch: function(button)
		{
			button.click( XenForo.memberMapAjax.doLocationSearch );
		},
		
		doLocationSearch: function(e)
		{
			geocoder = new google.maps.Geocoder();
			
			$( '#locationNoResults' ).fadeOut( XenForo.speed.fast );
			$( '#locationResultsWrapper' ).slideUp( XenForo.speed.fast, function()
			{
				$( '#locationResults' ).empty();
				
				geocoder.geocode( 
					{
						address: $( '#location' ).val()
					},
					function( results, status )
					{
						if ( status == google.maps.GeocoderStatus.OK )
						{
							$.each( results, function( i, result )
							{
								latLng = result.geometry.location.lat() + ',' + result.geometry.location.lng();
								
								$('<li>')
									.append( "<input type='radio' class='SubmitOnChange' name='locationToUse' value='" + latLng + "' /> " + result.formatted_address )
									.xfInsert( 'appendTo', '#locationResults' );
							});
							
							$( '#locationResultsWrapper' ).slideDown( XenForo.speed.fast );
						}
						else
						{
							$( '#locationResultsWrapper' ).hide();
							$( '#locationNoResults' ).show();
						}
					}
				);
			});
		},
		
		html5LocationSuccess: function(pos)
		{
			console.dir( pos );
			
			var lat = parseFloat( pos.coords.latitude );
			var lng = parseFloat( pos.coords.longitude );
			var latLng = lat + ',' + lng;

			if ( ! pos.address )
			{
				geocoder = new google.maps.Geocoder();
				
				geocoder.geocode(
					{
						'location': new google.maps.LatLng(lat, lng)
					},
					function( results, status )
					{
						if ( status == google.maps.GeocoderStatus.OK )
						{
							$.each( results, function( i, result )
							{
								latLng = result.geometry.location.lat() + ',' + result.geometry.location.lng();
								
								$('<li>')
									.append( "<input type='radio' class='SubmitOnChange' name='locationToUse' value='" + latLng + "' /> " + result.formatted_address + ' ' + phrases['html5Detected'] )
									.xfInsert( 'appendTo', '#locationResults' );
								$( '#locationResultsWrapper' ).slideDown( XenForo.speed.xfast );
							});
						}
					}
				);
			}
			else
			{
				var location = pos.address.city + ', ' + pos.address.region + ', ' + pos.address.countryCode + ' ' + phrases['html5Detected'];
				
				$('<li>')
					.append( "<input type='radio' class='SubmitOnChange' name='locationToUse' value='" + latLng + "' /> " + location )
					.xfInsert( 'appendTo', '#locationResults' );
				$( '#locationResultsWrapper' ).slideDown( XenForo.speed.xfast );
			}
			
		},
		html5LocationError: function(error)
		{
			console.dir( error );
		}
	};

	XenForo.register( 'input.Search', 'XenForo.memberMapAjax.locationSearch' );
	
	/* Doing it this way to prevent the form from submitting when hitting the enter key */
	$(document).ready( function() 
	{
		$(window).keydown( function(event)
		{
			if(event.keyCode == 13) 
			{
				event.preventDefault();
				XenForo.memberMapAjax.doLocationSearch(event);
				return false;
			}
		});
	});
	
	
	if (navigator.geolocation) 
	{
		navigator.geolocation.getCurrentPosition( XenForo.memberMapAjax.html5LocationSuccess, XenForo.memberMapAjax.html5LocationError );
	}
	else
	{
		XenForo.memberMapAjax.html5LocationError([ 'Not supported' ]);
	}
}
(jQuery, this, document);
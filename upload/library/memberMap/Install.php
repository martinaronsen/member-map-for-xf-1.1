<?php

class memberMap_Install
{
	public static function install()
	{
		$db = XenForo_Application::get('db');

 		$db->query("
			CREATE TABLE IF NOT EXISTS `memberMap` (
			  `marker_id` int(5) NOT NULL AUTO_INCREMENT,
			  `marker_type` enum('user','custom') NOT NULL DEFAULT 'user',
			  `user_id` int(10) NOT NULL,
			  `lat` float(10,6) NOT NULL,
			  `lon` float(10,6) NOT NULL,
			  PRIMARY KEY (marker_id),
			  UNIQUE KEY user_id (user_id)
			) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci
		");
		
 		/* Upgrade from 1.0.0/1.0.1 to 1.0.2+ */
		if ( ! $db->fetchRow('SHOW columns FROM memberMap WHERE Field = ?', 'marker_id') )
		{
			$db->query( "
				ALTER TABLE memberMap 
					ADD marker_id INT(5) NOT NULL AUTO_INCREMENT, 
					ADD marker_type enum( 'user','custom' ) NOT NULL DEFAULT 'user', 
					DROP PRIMARY KEY,
					ADD PRIMARY KEY (marker_id),
					ADD UNIQUE KEY user_id (user_id)
			" );
		}
		
		return true;
	}
}
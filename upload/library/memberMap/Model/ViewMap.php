<?php

class memberMap_Model_ViewMap extends XenForo_Model
{
	private $userFollowing = array();
	
	private $coordCache = array();
	private $finalMarkers = array();
	
	private $_max_lon = -179.00;
	private $_min_lon = 179.00;
	private $_max_lat = -89.00;
	private $_min_lat = 89.00;
	
	private $center_lon = null;
	private $center_lat = null;
	
	/**
	 * Get marker by User ID
	 * 
	 * @param int $id	User ID
	 */
	public function getMarkerById( $id )
	{
		$marker = $this->_getDb()->fetchRow("
			SELECT *
			FROM memberMap
			WHERE user_id = ?",
		
			$id 
		);
		
		return ( $marker === FALSE ) ? false : $marker;
	}
	
	/**
	 * Get all markers
	 */
	public function getMarkers()
	{
		$markers = $this->_getDb()->fetchAll("
			SELECT mm.*, xfu.*, xfup.location
			FROM memberMap mm
			LEFT JOIN xf_user xfu ON (xfu.user_id = mm.user_id)
			LEFT JOIN xf_user_profile xfup ON (xfu.user_id = xfup.user_id)"
		);
		
		return $this->formatMarkersForOutput( $markers );
	}
	
	/**
	 * Build and return permissions
	 */
	public function getPermissions()
	{
		return array( 
			'canView'	=> XenForo_Visitor::getInstance()->hasPermission( 'memberMap', 'memberMap_viewMap' ),
			'addMarker' => (XenForo_Visitor::getInstance()->hasPermission( 'memberMap', 'memberMap_addMarker' ) ? true : false ),
			'delMarker' => (XenForo_Visitor::getInstance()->hasPermission( 'memberMap', 'memberMap_deleteMarker' ) ? true : false),
		);

	}
	
	/**
	 * Format markers for output. Sort by username, and return as JSON
	 * 
	 * @param 	array 	$markers	From getMarkers()
	 */
	public function formatMarkersForOutput( array $markers )
	{
		$this->userFollowing = $this->getModelFromCache( 'XenForo_Model_User' )->getFollowedUserProfiles( XenForo_Visitor::getUserId() );

		foreach( $markers as $marker )
		{
			$this->addMarkerToArray( $marker );
		}
		
		/* Sort markers */
		ksort( $this->finalMarkers );
		
		return Zend_Json_Encoder::encode( $this->finalMarkers );
		//return XenForo_ViewRenderer_Json::jsonEncodeForOutput( $this->finalMarkers, false );
	}
	
	/**
	 * Add markers to final array. Adjust coords where needed
	 * 
	 * @param 	array 	$marker 	From formatMarkersForOutput()
	 */
	private function addMarkerToArray( $marker )
	{
		$lat = round( $marker['lat'], 4 );
		$lon = round( $marker['lon'], 4 );
		
		/* We don't want two markers on the exact same spot, do we? */
		if ( isset( $this->coordCache[ md5( "{$lat},{$lon}" ) ] ) )
		{
			$marker['lon'] += $this->randFloat( 0.1, 0.2 );
    		$marker['lat'] += $this->randFloat( 0.1, 0.2 );
    		
    		/* Retry */
    		return $this->addMarkerToArray( $marker );
		}
		
		/* Add final coords to internal cache */
		$this->coordCache[ md5( "{$lat},{$lon}" ) ] = array();
		
		/* Am I following this user, or is it a staff member? */
		$followedUser 	= ( isset( $this->userFollowing[ $marker['user_id'] ] ) ) ? true : false;
		$staffMember 	= ( $marker['is_admin'] OR $marker['is_moderator'] ) ? true : false;
		 
		$this->finalMarkers[ utf8_strtolower( $marker['username'] ) ] = array( 
			'latitude' 		=> $marker['lat'],
			'longitude' 	=> $marker['lon'],
			'username'		=> $marker['username'],
			'profileLink'	=> XenForo_Link::buildPublicLink( 'members', $marker ),
			'profilePic'	=> XenForo_Template_Helper_Core::helperAvatarUrl( $marker, 's', null, true ),
			'location'		=> $marker['location'],
			'followedUser'	=> $followedUser,
			'staffMember'	=> $staffMember,
		);
		
		/* Adjust center coords */
		$this->adjustCenterCoords( $marker['lat'], $marker['lon'] );
	}

    /**
     * Neat method to return random float
     */
	private function randFloat( $min, $max ) 
	{
		return ( $min + lcg_value() * ( abs( $max - $min ) ) );
	}
	
	/**
	 * Adjust map center coordinates by the given lat/lon point
	 * 
	 * @param 	string 	$lon 	Marker longitude (horizontal)
	 * @param 	string 	$lat 	Marker latitude (vertical)
	 */
	private function adjustCenterCoords( $lat, $lon ) 
	{
		if( strlen( (string)$lon ) == 0 OR strlen( (string)$lat ) == 0 )
		{
			return false;
		}
         
		$this->_max_lon = (float) max( $lon, $this->_max_lon );
		$this->_min_lon = (float) min( $lon, $this->_min_lon );
		$this->_max_lat = (float) max( $lat, $this->_max_lat );
		$this->_min_lat = (float) min( $lat, $this->_min_lat );
        
		$this->center_lon = (float) ( $this->_min_lon + $this->_max_lon ) / 2;
		$this->center_lat = (float) ( $this->_min_lat + $this->_max_lat ) / 2;
		
		return true;
	}
	
	/**
	 *  Fetch max/min lat/lon for output 
	*/
	public function getMaxMinCoords()
	{
		return array( 
			'max' => array( 
				'lat' => $this->_max_lat, 
				'lon' => $this->_max_lon 
			), 
			'min' => array( 
				'lat' => $this->_min_lat, 
				'lon' => $this->_min_lon 
			) 
		);
	}
}
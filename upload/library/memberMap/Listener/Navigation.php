<?php

class memberMap_Listener_Navigation
{
	/**
	 * Run this if we opted to grant this add-on its own tab
	 */
	public static function ownTab(array &$extraTabs, $selectedTabId)
	{
		$options = XenForo_Application::get('options');
		$perms = XenForo_Model::create( 'memberMap_Model_ViewMap' )->getPermissions();
		
		if ( $options->memberMap_navPosition == 'own_tab' AND $perms['canView'] === TRUE )
		{
			$extraTabs['memberMap'] = array(
				'title' => new XenForo_Phrase( 'memberMap_title' ),
				'href' => XenForo_Link::buildPublicLink( 'memberMap' ),
				'position' => 'middle',
				'selected' => ( $selectedTabId == 'memberMap' ),
			);
		}
	}
	
	/**
	 * Or this if we want to hide it as a subitem in the members tab :(
	 */
	public static function membersTab( $hookName, &$contents, array $hookParams, XenForo_Template_Abstract $template )
	{
		if ( $hookName == 'navigation_tabs_members' )
		{
			$perms = XenForo_Model::create( 'memberMap_Model_ViewMap' )->getPermissions();
		
			if ( XenForo_Application::get( 'options' )->memberMap_navPosition == 'members_tab' AND $perms['canView'] === TRUE )
			{
				$contents .= $template->create( 'memberMap_membersTabHook', $hookParams );
			}
		}
	}
}
<?php

class memberMap_DataWriter_Marker extends XenForo_DataWriter
{
	protected $_existingDataErrorPhrase = 'requested_page_not_found';

	protected function _getFields()
	{
		return array(
			'memberMap' => array(
				'marker_id'				=> array( 'type' => self::TYPE_UINT, 'autoIncrement' => true ),
				'marker_type'			=> array( 'type' => self::TYPE_STRING, 'allowedValues' => array('user', 'custom'), 'required' => true ),
				'user_id'				=> array( 'type' => self::TYPE_UINT, 'required' => false ),
				'lat'					=> array( 'type' => self::TYPE_FLOAT, 'required' => true ),
				'lon'					=> array( 'type' => self::TYPE_FLOAT, 'required' => true ),
			)
		);
	}

	protected function _getExistingData($data)
	{
		if ( ! $userId = $this->_getExistingPrimaryKey( $data, 'user_id' ) )
		{
			return false;
		}

		return array( 'memberMap' => $this->getModelFromCache( 'memberMap_Model_ViewMap' )->getMarkerById( $userId ) );
	}

	protected function _getUpdateCondition( $tableName )
	{
		return 'user_id = ' . $this->_db->quote( $this->getExisting( 'user_id' ) );
	}

	protected function _preSave()
	{
		if ( ! $this->_existingData AND ! $this->get( 'user_id' ) )
		{
			$visitor = XenForo_Visitor::getInstance();
			$this->set( 'user_id', $visitor['user_id'] );
		}
	}
}
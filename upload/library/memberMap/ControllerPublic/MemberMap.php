<?php

/**
 * Member Map for XenForo
 * 
 * @version 1.0.5
 * @author Martin Aronsen
 *
 */

class memberMap_ControllerPublic_MemberMap extends XenForo_ControllerPublic_Abstract
{
	public $perms = array();
	public $options = array();
	
	/**
	 * Method for drawing out map, the first page.
	 */
	public function actionIndex()
	{
		if ( $this->perms['canView'] === FALSE )
		{
			return $this->responseNoPermission();
		}
		
		$this->canonicalizeRequestUrl(
			XenForo_Link::buildPublicLink( 'memberMap' )
		);
		
		$viewParams = array(
			'markers' 				=> $this->getModelFromCache( 'memberMap_Model_ViewMap' )->getMarkers(),
			'mapMaxMin'				=> $this->getModelFromCache( 'memberMap_Model_ViewMap' )->getMaxMinCoords(),
			'useMarkerClusterer' 	=> ( $this->options->memberMap_markerClusterer ) ? true : false,
			'addOnVersion'			=> "1.0.5",
			'canAddMarker'			=> $this->perms['addMarker'],
			'addNavigation'			=> ( $this->options->memberMap_navPosition == 'members_tab' ) ? true : false,
			'zoomLevel'				=> ( $this->options->memberMap_zoomLevel >= 1 AND $this->options->memberMap_zoomLevel <= 21 ) ? $this->options->memberMap_zoomLevel : 2,
		);

		return $this->responseView( 'memberMap_ViewPublic_ViewMap', 'memberMap_viewMap', $viewParams );
	}
	
	/**
	 * Show the form for adding, editing or deleting our marker
	 */
	public function actionAddMarker()
	{
		if ( $this->perms['canView'] === FALSE OR $this->perms['addMarker'] === FALSE )
		{
			return $this->responseNoPermission();
		}
		
		// guests not allowed
		$this->_assertRegistrationRequired();
		
		// the user is the current visitor
		$user = XenForo_Visitor::getInstance()->toArray();
		
		$marker = $this->getModelFromCache( 'memberMap_Model_ViewMap' )->getMarkerById( $user['user_id'] );

		// put the data into an array to be passed to the view so the template can use it
		$viewParams = array(
			'location' => $user['location'],
			'showDeleteButton' => ( is_array( $marker ) AND count( $marker ) AND $this->perms['delMarker'] ) ? true : false,
		);

		return $this->responseView(
			'memberMap_ViewPublic_AddMarker',
			'memberMap_addMarker',
			$viewParams
		);
		
	}
	
	/**
	 * Search for our inputted location using Google Geocoder API
	 * 
	 * @deprecated	Using the tools built into Google Maps JS instead
	 */
	public function actionLocationsearch()
	{
		/* Guests not allowed */
		$this->_assertRegistrationRequired();
		
		try
		{
			echo mb_detect_encoding($this->_request->searchFor);
			$http = XenForo_Helper_Http::getClient( 'http://maps.googleapis.com/maps/api/geocode/json' );
			$http->setParameterGet( array(
				'address' 	=> rawurlencode( $this->_request->searchFor ),
				'sensor'	=> 'false',
			));

			$response = $http->request( 'GET' );

			$body = $response->getBody();
			
			if ( preg_match( '#^[{\[]#', $body ) )
			{
				$parts = json_decode( $body, true );
			}
		}
		catch ( Zend_Http_Client_Exception $e )
		{
			XenForo_Error::logException( $e, false );
			return false;
		}
		
		if ( is_array( $parts['results'] ) AND count( $parts['results'] ) )
		{
			$result = array();
			foreach( $parts['results'] as $part )
			{
				$result[] = array( 
					'latLng' => "{$part['geometry']['location']['lat']},{$part['geometry']['location']['lng']}", 
					'location' => $part['formatted_address']
				);
			}
			
			// put the data into an array to be passed to the view so the template can use it
			$viewParams = array(
				'results' => $result,
				'last_request' => $http->getLastRequest()
			);
		}
		else
		{
			$viewParams = array( 
				'noResults' => true,
				'last_request' => $http->getLastRequest() 
			);
		}

		return $this->responseView(
			'memberMap_ViewPublic_LocationSearch',
			'',
			$viewParams
		);
		
	}
	
	/**
	 * Add, edit, or delete our marker
	 */
	public function actionDoAddMarker()
	{
		if ( $this->perms['canView'] === FALSE OR $this->perms['addMarker'] === FALSE )
		{
			return $this->responseNoPermission();
		}
		
		// this action must be called via POST
		$this->_assertPostOnly();
		
		// guests not allowed
		$this->_assertRegistrationRequired();
		
		if ( $this->_request->delete )
		{
			if ( $this->perms['delMarker'] )
			{
				$writer = XenForo_DataWriter::create( 'memberMap_DataWriter_Marker' );
					
				if ( $marker = $this->getModelFromCache( 'memberMap_Model_ViewMap' )->getMarkerById( XenForo_Visitor::getUserId() ) )
				{
					$writer->setExistingData( $marker );

					$writer->delete();
					
					return $this->responseRedirect(
						XenForo_ControllerResponse_Redirect::SUCCESS,
						XenForo_Link::buildPublicLink( 'memberMap' )
					);
				}
			}
			
			/* Still here? Then you probably don't have permission to do this */
			return $this->responseNoPermission();
		}
		else
		{
			list( $lat, $lon ) = explode( ',', $this->_request->locationToUse );
	
			if ( is_numeric( $lat ) AND is_numeric( $lon ) )
			{
				$writer = XenForo_DataWriter::create( 'memberMap_DataWriter_Marker' );
				
				if ( $marker = $this->getModelFromCache( 'memberMap_Model_ViewMap' )->getMarkerById( XenForo_Visitor::getUserId() ) )
				{
					$writer->setExistingData( $marker );
				}
				
				$writer->set( 'marker_type', 'user' );
				$writer->set( 'lat', $lat );
				$writer->set( 'lon', $lon );
				$writer->save();
				
				
				return $this->responseRedirect(
					XenForo_ControllerResponse_Redirect::SUCCESS,
					XenForo_Link::buildPublicLink( 'memberMap' )
				);
			}
			else
			{
				return $this->responseError( "Incorrect coordinates. Please try again." );
			}
		}
	}
	
	public static function getSessionActivityDetailsForList(array $activities)
	{
		$output = array();
		foreach ($activities AS $key => $activity)
		{
			$output[ $key ] =  array( 
				"Viewing", 
				new XenForo_Phrase( 'memberMap_title' ), 
				XenForo_Link::buildPublicLink( 'memberMap' ), 
				false 
			);
		}
		
		return $output;
	}
	
	public function _preDispatch( $action )
	{
		parent::_preDispatch( $action );

		$this->perms 	= $this->getModelFromCache( 'memberMap_Model_ViewMap' )->getPermissions();
		$this->options 	= XenForo_Application::get( 'options' );
	}
}
<?php

class memberMap_Route_Prefix_MemberMap implements XenForo_Route_Interface
{
	public function match( $routePath, Zend_Controller_Request_Http $request, XenForo_Router $router )
	{
		$action = $router->resolveActionWithStringParam( $routePath, $request, 'Index' );
		
		//$action = $router->resolveActionWithIntegerParam($routePath, $request, 'id');
		
		$routeMatch = $router->getRouteMatch( 'memberMap_ControllerPublic_MemberMap', $action, 'memberMap', $routePath );
		
		return $routeMatch;
	}

	public function buildLink($originalPrefix, $outputPrefix, $action, $extension, $data, array &$extraParams)
	{
		return XenForo_Link::buildBasicLinkWithIntegerParam($outputPrefix, $action, $extension, $data, 'id');
	}
}